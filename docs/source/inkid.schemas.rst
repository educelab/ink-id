inkid.schemas package
=====================

Module contents
---------------

.. automodule:: inkid.schemas
   :members:
   :undoc-members:
   :show-inheritance:
