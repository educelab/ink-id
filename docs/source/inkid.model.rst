inkid.model package
===================

Submodules
----------

inkid.model.model module
------------------------

.. automodule:: inkid.model.model
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: inkid.model
   :members:
   :undoc-members:
   :show-inheritance:
